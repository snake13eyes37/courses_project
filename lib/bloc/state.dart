import 'package:equatable/equatable.dart';

abstract class BlocState extends Equatable {
  @override
  List<Object?> get props => [hashCode];
}
