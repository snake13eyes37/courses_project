import 'package:flutter/material.dart';

class AppConstants {
  AppConstants._();
  static const Duration defaultAnimationDuration = Duration(milliseconds: 500);
  static const Duration successCodeTimerDuration = Duration(seconds: 20);
  static GlobalKey<NavigatorState> materialKey = GlobalKey<NavigatorState>();
  static const String defaultPhonePrefix = "+";
  static const String defaultPhoneMask = "# ### ###-##-##";

}
