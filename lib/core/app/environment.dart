import 'package:app/bloc/logging_bloc_observer.dart';
import 'package:app/data/core/local/hive.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';
class Environment {
  static const bool _isProd = !kDebugMode && !kProfileMode;

  static const bool isHttpLoggingEnabled = !_isProd;
  static const bool isBlocLoggingEnabled = !_isProd;
  static const bool canUseCustomUrl = !_isProd;

  static Log log = _isProd ? Log() : _ConsolePrettyLog();

  static LoggingBlocObserver blocObserver = LoggingBlocObserver(log);

  static late PackageInfo packageInfo;

  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();
    packageInfo = await PackageInfo.fromPlatform();
    await HiveConfig.init();
    // Map<String, String?>? proxy = await SystemProxy.getProxySettings();
    //initDI(proxy);

    // Adding smoothness for gradients
    Paint.enableDithering = true;

    // ignore: unawaited_futures
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    // Future.delayed(const Duration(milliseconds: 250)).then((value) {
    //   SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    //     statusBarIconBrightness: Brightness.light,
    //     statusBarColor: AppColors.backgroundColor,
    //     systemNavigationBarColor: AppColors.backgroundColor,
    //    statusBarBrightness: Brightness.dark,
    //    systemNavigationBarIconBrightness: Brightness.dark,
    //   ));
    // });

    // await Firebase.initializeApp(options: Default);
    //
    // if (kDebugMode) {
    //   await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
    // }
    //
    // FirebaseCrashlytics.instance.crash();
  }

  // static Future<void> recordFlutterError(
  //     FlutterErrorDetails flutterErrorDetails,
  //     ) async {
  //   log.e(
  //     'RecordedFlutterError',
  //     flutterErrorDetails.exception,
  //     flutterErrorDetails.stack,
  //   );
  //   return FirebaseCrashlytics.instance.recordFlutterError(flutterErrorDetails);
  // }
  //
  static Future<void> recordError(dynamic exception, StackTrace stack) async {
    log.e('RecordedError', exception, stack);
    return;
  }
}

class Log {
  /// Log a message at level verbose.
  void v(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level debug.
  void d(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level info.
  void i(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level warning.
  void w(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level error.
  void e(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level wtf.
  void wtf(String message, [Object? error, StackTrace? stackTrace]) {}
}

class _ConsolePrettyLog implements Log {
  final logger = Logger(
    filter: ProductionFilter(),
    printer: PrettyPrinter(
      methodCount: 0,
      lineLength: 240,
      printEmojis: true,
    ),
  );

  @override
  void d(message, [error, StackTrace? stackTrace]) {
    logger.d(message, error, stackTrace);
  }

  @override
  void e(message, [error, StackTrace? stackTrace]) {
    logger.e(message, error, stackTrace);
  }

  @override
  void i(message, [error, StackTrace? stackTrace]) {
    logger.i(message, error, stackTrace);
  }

  @override
  void v(message, [error, StackTrace? stackTrace]) {
    logger.v(message, error, stackTrace);
  }

  @override
  void w(message, [error, StackTrace? stackTrace]) {
    logger.w(message, error, stackTrace);
  }

  @override
  void wtf(message, [error, StackTrace? stackTrace]) {
    logger.wtf(message, error, stackTrace);
  }
}
