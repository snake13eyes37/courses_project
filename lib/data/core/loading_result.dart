import 'dart:async';

import 'package:app/data/core/mapper.dart';

class LoadingResult<T> {
  LoadingResult._();

  factory LoadingResult.error(Exception exception) => Error(exception);

  factory LoadingResult.success(T? data) => Success(data);

  /// Данный метод позволяет избавиться от свзяки `if else`, при работе с результатом запросов.
  ///
  /// Метод имеет две callback функции для успешной и исключительной ситации.
  ///
  ///
  /// [success] возвращает данные с типом [T], который определяется на основе [LoadingResult<T>],
  /// к которому была применен данный метод.
  ///
  /// [error] возвращает данные с типом [Exception], который был получен в следствии усключительной ситуации.
  ///
  /// Ниже представлен пример исаользования данного метода
  /// ```
  /// Пример:
  /// ```
  /// final result = await _searchFacilitiesByParamsUseCase.call(
  ///       state.searchRequestQueries,
  /// );
  ///
  /// result.when(
  ///       success: (data) { print(data.toString); },
  ///       error: (resultException) { print(resultException.toString()); },
  /// );
  /// ```
  FutureOr<void> when<R>({
    required Function(T?) success,
    Function(Exception)? error,
  }) async {
    final payload = this;
    if (payload is Success<T>) {
      return await success(payload.data);
    } else if (payload is Error<T>) {
      if (error != null) {
        return await error(payload.exception);
      }
    }
  }
}

class Error<T> extends LoadingResult<T> {
  final Exception exception;

  Error(this.exception) : super._();
}

class Success<T> extends LoadingResult<T> {
  final T? data;

  Success(this.data) : super._();
}

/// Данный класс используется в качестве заглушки пустого Exception
class IgnoreException implements Exception {
  IgnoreException();

  @override
  String toString() {
    return "";
  }
}


Future<LoadingResult<V>> executor<T, V>(
  Future<T> task,
  Mapper<V, T>? mapper,
) async {
  try {
    var result = await task;
    return LoadingResult.success(mapper?.call(result));
  } on Exception catch (e) {
    return LoadingResult.error(e);
  }
}
