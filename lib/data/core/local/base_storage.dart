
import 'package:hive/hive.dart';

class BaseStorage<T> {
  final String key;
  final Future<Box<T>> Function() boxProvider;

  BaseStorage({
    required this.key,
    required this.boxProvider,
  });

  Future<Box<T>> get _getBox => boxProvider.call();

  Future<T?> loadData() => _getBox.then((box) => box.get(key, defaultValue: null));

  Future<void> saveData(T dto) => _getBox.then((box) => box.put(key, dto));

  Future<void> clear() => _getBox.then((box) => box.clear());
}