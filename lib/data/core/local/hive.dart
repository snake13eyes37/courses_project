import 'dart:io';

import 'package:app/data/features/dishes/model/calories_hive_dto.dart';
import 'package:app/data/features/dishes/model/dishes_hive_dto.dart';
import 'package:app/data/features/dishes/storage/dish_storage.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:app/data/features/profile/storage/user_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path/path.dart' as path_helper;
import 'package:path_provider/path_provider.dart';

abstract class HiveConfig {
  static const String hiveDir = "Hive";

  static Future<void> init() async {
    await _initFlutter(hiveDir);
    _registerAdapters();
  }

  static void _registerAdapters() {
    Hive.registerAdapter(UserHiveDtoAdapter());
    Hive.registerAdapter(DishesHiveDtoAdapter());
    Hive.registerAdapter(CaloriesHiveDtoAdapter());
  }

  static Future<Box<UserHiveDto>> openUserBox() =>
      Hive.openBox<UserHiveDto>(UserStorage.userKey);

//
  static Future<Box<List<DishesHiveDto>>> openDishBox() {
    return Hive.openBox(DishStorage.dishKey);
  }
//
// static Future<Box<UserHiveDto>> openUserBox() {
//   return Hive.openBox(UserStorage.userKey);
// }
}

/// Hive Types IDs
/// Make sure to use typeIds consistently. Your changes have to be compatible to previous versions of the box.
/// [Hive doc](https://docs.hivedb.dev/#/custom-objects/type_adapters)
abstract class HiveTypeId {
  static const int user = 1;
  static const int dish = 2;
  static const int calories = 3;
}

/// From flutter_hive
Future _initFlutter([String? subDir]) async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    var libDir = Platform.isIOS
        ? await getLibraryDirectory()
        : await getApplicationSupportDirectory();
    var path = libDir.path;
    await checkIsOldDbExists();
    if (subDir != null) {
      path = path_helper.join(path, subDir);
    }
    Hive.init(path);
  }
}

Future<void> checkIsOldDbExists() async {
  var docDir = await getApplicationDocumentsDirectory();
  Directory(docDir.path).list().forEach((element) async {
    if (element.path.contains(".hive") || element.path.contains(".lock")) {
      await Directory(element.path).delete(recursive: true);
    }
  });
}
