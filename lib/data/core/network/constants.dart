class ApiConstants {
  static const Duration defaultTokenExpirationTime = Duration(minutes: 1);
  static const Duration defaultConnectTimeout = Duration(seconds: 5);
  static const Duration defaultReceiveTimeout = Duration(seconds: 10);
  static const Duration defaultSendTimeout = Duration(seconds: 10);
}
