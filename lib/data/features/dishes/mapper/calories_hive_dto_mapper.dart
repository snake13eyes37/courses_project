import 'package:app/data/core/core.dart';
import 'package:app/data/features/dishes/model/calories_hive_dto.dart';
import 'package:app/data/features/dishes/model/dishes_hive_dto.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:app/domain/calories/calories_model.dart';
import 'package:app/domain/dish/dish_model.dart';
import 'package:app/domain/profile/user_model.dart';

class CaloriesHiveDtoMapper implements Mapper<CaloriesModel, CaloriesHiveDto> {
  @override
  CaloriesModel call(CaloriesHiveDto data) {
    return CaloriesModel(
      proteins: data.protein,
      fats: data.fats,
      carbohydrates: data.carbohydrates,
    );
  }

  CaloriesHiveDto toDto(CaloriesModel domain) {
    return CaloriesHiveDto(
      protein: domain.proteins,
      fats: domain.fats,
      carbohydrates: domain.carbohydrates,
    );
  }
}
