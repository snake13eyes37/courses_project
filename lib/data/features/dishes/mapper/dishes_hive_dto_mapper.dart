import 'package:app/data/core/core.dart';
import 'package:app/data/features/dishes/mapper/calories_hive_dto_mapper.dart';
import 'package:app/data/features/dishes/model/dishes_hive_dto.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:app/domain/dish/dish_model.dart';
import 'package:app/domain/profile/user_model.dart';

class DishesHiveDtoMapper implements Mapper<DishModel, DishesHiveDto> {
  @override
  DishModel call(DishesHiveDto data) {
    return DishModel(
      name: data.name,
      gram: data.gram,
      photo: data.photo,
      calories: CaloriesHiveDtoMapper().call(data.calories),
    );
  }

  DishesHiveDto toDto(DishModel domain) {
    return DishesHiveDto(
      name: domain.name,
      gram: domain.gram,
      photo: domain.photo,
      calories: CaloriesHiveDtoMapper().toDto(domain.calories),
    );
  }
}
