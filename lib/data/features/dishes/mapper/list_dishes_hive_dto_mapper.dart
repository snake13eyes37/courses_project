import 'package:app/data/core/core.dart';
import 'package:app/data/features/dishes/mapper/dishes_hive_dto_mapper.dart';
import 'package:app/data/features/dishes/model/dishes_hive_dto.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:app/domain/dish/dish_model.dart';
import 'package:app/domain/profile/user_model.dart';

class ListDishesHiveDtoMapper
    implements Mapper<List<DishModel>, List<DishesHiveDto>> {
  @override
  List<DishModel> call(List<DishesHiveDto> data) {
    return data.map(DishesHiveDtoMapper().call).toList();
  }

  List<DishesHiveDto> toDto(List<DishModel> domain) {
    return domain.map(DishesHiveDtoMapper().toDto).toList();
  }
}
