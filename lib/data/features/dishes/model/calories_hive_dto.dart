import 'package:app/data/core/local/hive.dart';
import 'package:hive/hive.dart';

part 'calories_hive_dto.g.dart';

@HiveType(typeId: HiveTypeId.calories)
class CaloriesHiveDto {
  @HiveField(0)
  final double protein;
  @HiveField(1)
  final double fats;
  @HiveField(2)
  final double carbohydrates;

  const CaloriesHiveDto({
    required this.protein,
    required this.fats,
    required this.carbohydrates,
  });
}
