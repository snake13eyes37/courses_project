// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'calories_hive_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CaloriesHiveDtoAdapter extends TypeAdapter<CaloriesHiveDto> {
  @override
  final int typeId = 3;

  @override
  CaloriesHiveDto read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CaloriesHiveDto(
      protein: fields[0] as double,
      fats: fields[1] as double,
      carbohydrates: fields[2] as double,
    );
  }

  @override
  void write(BinaryWriter writer, CaloriesHiveDto obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.protein)
      ..writeByte(1)
      ..write(obj.fats)
      ..writeByte(2)
      ..write(obj.carbohydrates);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CaloriesHiveDtoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
