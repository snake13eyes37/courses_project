import 'package:app/data/core/local/hive.dart';
import 'package:app/data/features/dishes/model/calories_hive_dto.dart';
import 'package:hive/hive.dart';

part 'dishes_hive_dto.g.dart';

@HiveType(typeId: HiveTypeId.dish)
class DishesHiveDto {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final double gram;
  @HiveField(2)
  final String photo;
  @HiveField(3)
  final CaloriesHiveDto calories;

  const DishesHiveDto({
    required this.name,
    required this.gram,
    required this.photo,
    required this.calories,
  });
}
