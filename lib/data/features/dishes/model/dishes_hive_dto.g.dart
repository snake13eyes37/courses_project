// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dishes_hive_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DishesHiveDtoAdapter extends TypeAdapter<DishesHiveDto> {
  @override
  final int typeId = 2;

  @override
  DishesHiveDto read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DishesHiveDto(
      name: fields[0] as String,
      gram: fields[1] as double,
      photo: fields[2] as String,
      calories: fields[3] as CaloriesHiveDto,
    );
  }

  @override
  void write(BinaryWriter writer, DishesHiveDto obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.gram)
      ..writeByte(2)
      ..write(obj.photo)
      ..writeByte(3)
      ..write(obj.calories);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DishesHiveDtoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
