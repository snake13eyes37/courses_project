import 'package:app/data/core/loading_result.dart';
import 'package:app/data/features/dishes/mapper/list_dishes_hive_dto_mapper.dart';
import 'package:app/data/features/dishes/storage/dish_storage.dart';
import 'package:app/data/features/profile/mapper/user_hive_dto_mapper.dart';
import 'package:app/data/features/profile/storage/user_storage.dart';
import 'package:app/domain/dish/dish_model.dart';
import 'package:app/domain/profile/user_model.dart';

abstract class DishRepository {
  Future<LoadingResult<List<DishModel>>> getDishes();
}

class DishRepositoryImpl implements DishRepository {
  final DishStorage _storage;

  DishRepositoryImpl(this._storage);

  @override
  Future<LoadingResult<List<DishModel>>> getDishes() {
    return executor(
      _storage.loadAuthData(),
      ListDishesHiveDtoMapper(),
    );
  }
}
