import 'package:app/data/core/core.dart';
import 'package:app/data/features/dishes/model/dishes_hive_dto.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:hive/hive.dart';

class DishStorage {
  static const dishKey = "dish";

  Box<List<DishesHiveDto>>? dishesBox;

  Future<Box<List<DishesHiveDto>>> _getBox() async {
    return dishesBox ??= await HiveConfig.openDishBox();
  }

  Future<List<DishesHiveDto>?> loadAuthData() =>
      _getBox().then((box) => box.get(dishesBox, defaultValue: null));

  Future<void> saveAuthData(List<DishesHiveDto> dishesList) =>
      _getBox().then((box) => box.put(dishKey, dishesList));

  Future<void> clear() => _getBox().then((box) => box.clear());
}
