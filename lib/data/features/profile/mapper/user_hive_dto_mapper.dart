import 'package:app/data/core/core.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:app/domain/profile/user_model.dart';

class UserHiveDtoMapper implements Mapper<UserModel, UserHiveDto> {
  @override
  UserModel call(UserHiveDto data) {
    return UserModel(
      name: data.name,
      lastName: data.lastName,
      years: data.years,
      height: data.height,
      weight: data.weight,
    );
  }

  UserHiveDto toDto(UserModel domain) {
    return UserHiveDto(
      name: domain.name,
      lastName: domain.lastName,
      years: domain.years,
      height: domain.height,
      weight: domain.weight,
    );
  }
}
