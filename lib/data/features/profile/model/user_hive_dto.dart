import 'package:app/data/core/local/hive.dart';
import 'package:hive/hive.dart';

part 'user_hive_dto.g.dart';

@HiveType(typeId: HiveTypeId.user)
class UserHiveDto {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final String lastName;
  @HiveField(2)
  final int years;
  @HiveField(3)
  final double height;
  @HiveField(4)
  final double weight;

  UserHiveDto({
    required this.name,
    required this.lastName,
    required this.years,
    required this.height,
    required this.weight,
  });
}
