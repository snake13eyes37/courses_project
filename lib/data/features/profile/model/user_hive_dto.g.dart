// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_hive_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserHiveDtoAdapter extends TypeAdapter<UserHiveDto> {
  @override
  final int typeId = 1;

  @override
  UserHiveDto read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserHiveDto(
      name: fields[0] as String,
      lastName: fields[1] as String,
      years: fields[2] as int,
      height: fields[3] as double,
      weight: fields[4] as double,
    );
  }

  @override
  void write(BinaryWriter writer, UserHiveDto obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.lastName)
      ..writeByte(2)
      ..write(obj.years)
      ..writeByte(3)
      ..write(obj.height)
      ..writeByte(4)
      ..write(obj.weight);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserHiveDtoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
