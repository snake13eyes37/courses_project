import 'package:app/data/core/loading_result.dart';
import 'package:app/data/features/profile/mapper/user_hive_dto_mapper.dart';
import 'package:app/data/features/profile/storage/user_storage.dart';
import 'package:app/domain/profile/user_model.dart';

abstract class UserRepository {
  Future<LoadingResult<UserModel>> getUser();
}

class UserRepositoryImpl implements UserRepository {
  final UserStorage _storage;

  UserRepositoryImpl(this._storage);

  @override
  Future<LoadingResult<UserModel>> getUser() {
    return executor(
      _storage.loadAuthData(),
      UserHiveDtoMapper(),
    );
  }
}
