import 'package:app/data/core/core.dart';
import 'package:app/data/features/profile/model/user_hive_dto.dart';
import 'package:hive/hive.dart';

class UserStorage {
  static const userKey = "user";

  Box<UserHiveDto>? userBox;

  Future<Box<UserHiveDto>> _getBox() async {
    return userBox ??= await HiveConfig.openUserBox();
  }

  Future<UserHiveDto?> loadAuthData() =>
      _getBox().then((box) => box.get(userKey, defaultValue: null));

  Future<void> saveAuthData(UserHiveDto newUser) =>
      _getBox().then((box) => box.put(userKey, newUser));

  Future<void> clear() => _getBox().then((box) => box.clear());
}
