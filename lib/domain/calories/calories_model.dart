class CaloriesModel{
  final  double proteins;
  final double fats;
  final double carbohydrates;

//<editor-fold desc="Data Methods">
  const CaloriesModel({
    required this.proteins,
    required this.fats,
    required this.carbohydrates,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CaloriesModel &&
          runtimeType == other.runtimeType &&
          proteins == other.proteins &&
          fats == other.fats &&
          carbohydrates == other.carbohydrates);

  @override
  int get hashCode =>
      proteins.hashCode ^ fats.hashCode ^ carbohydrates.hashCode;

  @override
  String toString() {
    return 'CaloriesModel{' +
        ' proteins: $proteins,' +
        ' fats: $fats,' +
        ' carbohydrates: $carbohydrates,' +
        '}';
  }

  CaloriesModel copyWith({
    double? proteins,
    double? fats,
    double? carbohydrates,
  }) {
    return CaloriesModel(
      proteins: proteins ?? this.proteins,
      fats: fats ?? this.fats,
      carbohydrates: carbohydrates ?? this.carbohydrates,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'proteins': this.proteins,
      'fats': this.fats,
      'carbohydrates': this.carbohydrates,
    };
  }

  factory CaloriesModel.fromMap(Map<String, dynamic> map) {
    return CaloriesModel(
      proteins: map['proteins'] as double,
      fats: map['fats'] as double,
      carbohydrates: map['carbohydrates'] as double,
    );
  }

//</editor-fold>
}