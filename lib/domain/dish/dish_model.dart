import 'package:app/domain/calories/calories_model.dart';

class DishModel{
  final String name;
  final double gram;
  final String photo;
  final CaloriesModel calories;

//<editor-fold desc="Data Methods">
  const DishModel({
    required this.name,
    required this.gram,
    required this.photo,
    required this.calories,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DishModel &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          gram == other.gram &&
          photo == other.photo &&
          calories == other.calories);

  @override
  int get hashCode =>
      name.hashCode ^ gram.hashCode ^ photo.hashCode ^ calories.hashCode;

  @override
  String toString() {
    return 'DishModel{' +
        ' name: $name,' +
        ' gram: $gram,' +
        ' photo: $photo,' +
        ' calories: $calories,' +
        '}';
  }

  DishModel copyWith({
    String? name,
    double? gram,
    String? photo,
    CaloriesModel? calories,
  }) {
    return DishModel(
      name: name ?? this.name,
      gram: gram ?? this.gram,
      photo: photo ?? this.photo,
      calories: calories ?? this.calories,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'gram': this.gram,
      'photo': this.photo,
      'calories': this.calories,
    };
  }

  factory DishModel.fromMap(Map<String, dynamic> map) {
    return DishModel(
      name: map['name'] as String,
      gram: map['gram'] as double,
      photo: map['photo'] as String,
      calories: map['calories'] as CaloriesModel,
    );
  }

//</editor-fold>
}