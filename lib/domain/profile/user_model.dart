class UserModel {
  final String name;
  final String lastName;
  final int years;
  final double height;
  final double weight;

//<editor-fold desc="Data Methods">
  const UserModel({
    required this.name,
    required this.lastName,
    required this.years,
    required this.height,
    required this.weight,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is UserModel &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          lastName == other.lastName &&
          years == other.years &&
          height == other.height &&
          weight == other.weight);

  @override
  int get hashCode =>
      name.hashCode ^
      lastName.hashCode ^
      years.hashCode ^
      height.hashCode ^
      weight.hashCode;

  @override
  String toString() {
    return 'UserModel{' +
        ' name: $name,' +
        ' lastName: $lastName,' +
        ' years: $years,' +
        ' height: $height,' +
        ' weight: $weight,' +
        '}';
  }

  UserModel copyWith({
    String? name,
    String? lastName,
    int? years,
    double? height,
    double? weight,
  }) {
    return UserModel(
      name: name ?? this.name,
      lastName: lastName ?? this.lastName,
      years: years ?? this.years,
      height: height ?? this.height,
      weight: weight ?? this.weight,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'lastName': this.lastName,
      'years': this.years,
      'height': this.height,
      'weight': this.weight,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      name: map['name'] as String,
      lastName: map['lastName'] as String,
      years: map['years'] as int,
      height: map['height'] as double,
      weight: map['weight'] as double,
    );
  }

//</editor-fold>
}