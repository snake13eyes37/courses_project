import 'package:app/core/app/constants.dart';
import 'package:app/presentation/app/theme.dart';
import 'package:app/ui/core/colors.dart';
import 'package:app/ui/locale/generated/l10n.dart';
import 'package:app/ui/utils/scroll_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class AppWidget extends StatefulWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  State<AppWidget> createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: AppConstants.materialKey,
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      theme: AppTheme.appTheme,
      supportedLocales: S.delegate.supportedLocales,
      builder: (context, child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: Stack(
            children: [
              ScrollConfiguration(
                behavior: ScrollConfig(),
                child: child ?? const SizedBox.shrink(),
              ),
            ],
          ),
        );
      },
      home: const Scaffold(
        backgroundColor: AppColors.backgroundColor,
        body: SizedBox(),
      ),
    );
  }
}
