import 'package:app/ui/core/colors.dart';
import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static ThemeData appTheme = ThemeData(
      primaryColor: AppColors.backgroundColor,
      primaryColorLight: AppColors.black,
      scaffoldBackgroundColor: AppColors.white,
      backgroundColor: AppColors.white,
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          shape:
              MaterialStateProperty.all<OutlinedBorder>(const StadiumBorder()),
          foregroundColor: MaterialStateProperty.all<Color>(
            AppColors.white,
          ),
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) {
              if (states.contains(MaterialState.disabled)) {
                return AppColors.white;
              }
              return AppColors.backgroundColor;
            },
          ),
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          ),
          side: MaterialStateProperty.all(
            const BorderSide(
              width: 0.5,
              color: AppColors.black,
            ),
          ),
          elevation: MaterialStateProperty.all(0),
          padding: MaterialStateProperty.all(
            const EdgeInsets.symmetric(
              horizontal: 24,
              vertical: 16,
            ),
          ),
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          foregroundColor: MaterialStateProperty.all(AppColors.black),
        ),
      ),
      canvasColor: AppColors.white,
      colorScheme: const ColorScheme.light().copyWith(
        primary: AppColors.backgroundColor,
      ),
      appBarTheme: const AppBarTheme(elevation: 0));
}
