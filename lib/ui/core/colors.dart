import 'package:flutter/material.dart';

class AppColors {
  const AppColors._();

  static const Color transparent = Colors.transparent;
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0x00000000);
  static const Color backgroundColor = Color(0xFF050b0f);
  static const Color orange = Color(0xFFff5c00);
  static const Color alert = Color(0xFFff2222);
  static const Color green = Color(0xFF28AE07);
  static const Color dark = Color(0xFF445058);
  static const Color dark70 = Color(0xB3445058);
  static const Color mistyWhite = Color(0xFFd6dbdf);
  static const Color textFieldBorderColor = Color(0x2F3F4A33);
  static const Color redError = Color(0xFFe93030);
  static const Color gradientEnd = Color(0xFFfb5f05);
  static const Color gradientStart = Color(0xFF35d2de);
  static const Color black90 = Color(0xE6000000);
  static const Color checkGray = Color(0xFFEDEDED);


  static const Gradient gradient = LinearGradient(
    colors: [
      gradientStart,
      gradientEnd,
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
    stops: [
      0.4,
      0.6,
    ],
    transform: GradientRotation(-1.25),
  );
  static const Gradient qrGradient = LinearGradient(
    colors: [
      gradientStart,
      gradientEnd,
    ],
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
    // stops: [
    //   0.4,
    //   0.6,
    // ],
  );

}
