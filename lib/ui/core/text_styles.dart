import 'package:app/ui/core/colors.dart';
import 'package:flutter/cupertino.dart';

class AppTextStyles {
  AppTextStyles._();

  static const _fontFamily = "Manrope";

  static const _cormorantGaramond = "CormorantGaramond";

  static const TextStyle semiBold22 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 22,
  );

  static const TextStyle semiBold18 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 18,
  );

  static const TextStyle semiBold16 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 16,
  );

  static const TextStyle semiBold12 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 12,
  );

  static const TextStyle medium14 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 14,
  );

  static const TextStyle medium10 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 10,
  );

  static const TextStyle medium8 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 8,
  );

  static const TextStyle light50 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w300,
    fontSize: 50,
  );

  static const TextStyle light36 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w300,
    fontSize: 36,
  );

  static const TextStyle light34 = TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w300,
    fontSize: 34,
  );

  static const TextStyle cormorantRegular42 = TextStyle(
    fontFamily: _cormorantGaramond,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 42,
  );
}
