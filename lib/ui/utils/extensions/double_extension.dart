import 'package:intl/intl.dart';

extension DoubleExt on double {
  String get formatPrice {
    var formatter = NumberFormat.decimalPattern('ru_RU');
    return formatter.format(this);
  }

  String get withoutZeroFraction {
    RegExp regex = RegExp(r'([.]*0+)(?!.*\d)');
    return toString().replaceAll(regex, '');
  }

  String get doubleWithoutDecimalToInt {
    return toStringAsFixed(truncateToDouble() == this ? 0 : 1);
  }
}

extension DoubleNullableExt on double? {
  bool get isNullOrZero => this == null || this == 0;

  String totalPrice(double? discount) {
    final price = this;
    if (price == null) return '';
    if (discount == null) return price.formatPrice;
    return (price * ((100 - discount) / 100)).formatPrice;
  }
}