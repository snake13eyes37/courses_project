import 'package:app/core/app/environment.dart';

extension IterableExt<T> on Iterable<T> {
  printAll() {
    forEach((element) {
      Environment.log.d('$element');
    });
  }

  void forEachIndexed(void Function(T e, int i) f) {
    var i = 0;
    forEach((e) => f(e, i++));
  }

  Iterable<E> mapIndexed<E>(E Function(T e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }

  T? firstWhereOrNull(bool Function(T e) f) {
    for (T element in this) {
      if (f(element)) return element;
    }
    return null;
  }

  T? get tryFirst => ((length) > 0 ? first : null);
}
