extension ListExt<T> on List<T> {
  List<T>? get nullIfEmpty {
    return isEmpty ? null : this;
  }

  T firstOrDefault(T defaultValue) {
    return isNotEmpty ? first : defaultValue;
  }
}

extension ListNullableExt<T> on List<T>? {
  List<T> addAndReturn(T value) {
    var list = this;
    if (list == null) {
      return List.of([value]);
    }
    list.add(value);
    return list;
  }
}

extension ListStringExt on List<String> {
  String get firstOrEmpty => length > 0 ? first : '';
}
