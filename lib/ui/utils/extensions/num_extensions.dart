import 'package:intl/intl.dart';

extension IntExtensions on num{
  String get formatPrice{
    var formatter = NumberFormat.decimalPattern('ru_RU');
    return formatter.format(this);
  }
}