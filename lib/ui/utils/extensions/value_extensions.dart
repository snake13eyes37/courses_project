String valueOrEmpty(String? value, {String defaultValue = ''}) =>
    value ?? defaultValue;

T valueOrDefault<T>(T? value, T defaultValue) => value ?? defaultValue;

String? notEmptyValueOrNull(String value) => value.isEmpty ? null : value;

String? nullValueIfEmpty(String? value) =>
    value == null || value.isEmpty ? null : value;

List<T> valueOrEmptyList<T>(List<T>? list) => list ?? [];

int valueOrZero(int? value) => value ?? 0;

double valueOrDoubleZero(double? value) => value ?? 0.0;

int valueDoubleToIntOrZero(double? value) => value?.toInt() ?? 0;

bool valueOrFalse(bool? flag) => flag ?? false;

extension StringExt on String {
  String get capitalize {
    if (isEmpty) {
      return this;
    }
    var string = toLowerCase();
    final result = string[0].toUpperCase() + string.substring(1);
    return result;
  }

  bool get isNumeric {
    return int.tryParse(this) != null;
  }

  String get removePrefixSymbol {
    return replaceAll('+', '');
  }

  bool firstSymbolIsEquals(String symbol) {
    if (isEmpty) return false;
    return this[0] == symbol;
  }
}
